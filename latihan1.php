<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Latihan 1 Internal CSS</title>
	<style type="text/css">
		body{
			background-color: darkred;
		}

		/*h1{
			color : white;
			text-align: center;
		}*/

		p{
			font-family: arial;
			font-size: 25px;
		}

		.myClass{
			font: bold 1.25em times;
			color: white;
		}
	</style>
</head>
<body>
	Berikut adalah latihan CSS

	<h1>Komputerisasi Akuntansi</h1>

	<p>Politeknik Negeri Semarang</p>

	<h1 class="myClass">Kelas JWD</h1>
	<p class="myClass">Oleh Pak Amran Yobioktabera</p>
</body>
</html>