<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>CSS Eksternal</title>
	<link rel="stylesheet" type="text/css" href="mystyle.css">
</head>
<body>
	<h1>Politeknik Negeri Semarang</h1>
	<h2>About Us</h2>
	<h3>Lembaga Pendidikan ini didirikan atas dasar langkanya tenaga teknisi ahli madya yang diperlukan oleh industri dan keberhasilan Politeknik mekanik Swiss dan ITB, yang didirikan pada tahun 1976. Untuk itu tahun 1982 telah dioperasikan 6 buah Politeknik di USU Medan, UNSRI Palembang, UI Jakarta, ITB Bandung, UNDIP Semarang, dan UNIBRAW Malang dengan bantuan Bank Dunia sesuai dengan surat keputusan Direktorat Jenderal Pendidikan Tinggi Nomor 03/Dj/Kep/1979.
<br><br>
Politeknik Universitas Diponegoro pada awal mulanya (1982) mempunyai tiga departemen/jurusan bidang keteknikan, yaitu, Departemen/Jurusan Teknik Sipil, Departemen/Jurusan Teknik Mesin, dan Departemen /Jurusan Teknik Elektro. Tahun 1985 dibuka Jurusan Tata Niaga karena kebutuhan industri menuntut adanya tenaga terampil di bidang bisnis. Pada tahun 1989 terjadi pengembangan jurusan Elektro menjadi Jurusan Teknik Listrik dan Jurusan Teknik Elektronika/Telekomunikasi.
<br> <br>
Berdasarkan Surat Keputusan Menteri Pendidikan dan Kebudayaan RI No. : 313/O/1991 tanggal 6 Juni 1991 tentang Penataan Politeknik dalam lingkungan Universitas dan Institut Negeri maka pada tahun 1992 mempunyai 5 jurusan yaitu : Teknik Sipil, Teknik Mesin, Teknik Elektro, Akuntansi dan Administrasi Bisnis.
<br><br>
Pada tanggal 6 Agustus tahun 1997 Politeknik Universitas Diponegoro dinyatakan mandiri dan lepas dari manajemen Universitas Diponegoro dengan Surat Keputusan Menteri Pendidikan dan Kebudayaan RI Nomor : 175/O/1997 dengan nama POLITEKNIK NEGERI SEMARANG. Kemudian pada tanggal 31 Juli 2002 terbit surat keputusan Menteri Pendidikan Nasional nomor 134/O/2002 mengatur tentang Organisasi dan Tata Kerja Politeknik Negeri Semarang.</h3>
</body>
</html>