<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Javascript IF/ELSE</title>
</head>
<body>
	<script type="text/javascript">
		var password = prompt("Kata Kunci:");

		if(password=="Polines123"){
			document.write("<h2>Selamat datang di website kami</h2>");
		}else{
			document.write("<h2>Password salah, coba lagi</h2>");
		}

		document.write("<p>Kunjungi media sosial kami yang lain</p>");
	</script>
</body>
</html>